package com.project.mfronczak.tasklist.model;

public enum Priority {
    MINOR, NORMAL, MAJOR
}
