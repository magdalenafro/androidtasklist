package com.project.mfronczak.tasklist.model;

public enum Status {
    WIP, TODO, DONE
}
