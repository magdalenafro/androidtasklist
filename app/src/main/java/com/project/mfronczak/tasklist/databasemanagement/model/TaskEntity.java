package com.project.mfronczak.tasklist.databasemanagement.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(tableName = "tasks")
public class TaskEntity implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Integer id;
    @ColumnInfo(name = "name_of_task")
    public String nameOfTask;
    public String status;
    @ColumnInfo(name = "added_date")
    public Integer addedDate;
    @ColumnInfo(name = "planned_end")
    public Integer plannedEnd;
    public String priority;
    public String notes;
    @ColumnInfo(name = "attachment_list")
    public String attachmentList;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Integer addedDate) {
        this.addedDate = addedDate;
    }

    public Integer getPlannedEnd() {
        return plannedEnd;
    }

    public void setPlannedEnd(Integer plannedEnd) {
        this.plannedEnd = plannedEnd;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(String attachmentList) {
        this.attachmentList = attachmentList;
    }
}
