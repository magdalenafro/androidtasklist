package com.project.mfronczak.tasklist.databasemanagement.model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TaskDao {

    @Insert
    void insert(TaskEntity taskEntity);

    @Update
    void update(TaskEntity taskEntity);

    @Query("SELECT * from tasks ORDER BY id ASC")
    LiveData<List<TaskEntity>> getAllTasks();

    @Delete
    void delete(TaskEntity taskEntity);

}
