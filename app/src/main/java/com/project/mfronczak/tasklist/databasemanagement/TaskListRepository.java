package com.project.mfronczak.tasklist.databasemanagement;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.project.mfronczak.tasklist.databasemanagement.model.TaskDao;
import com.project.mfronczak.tasklist.databasemanagement.model.TaskEntity;

import java.util.List;

public class TaskListRepository {
    private TaskDao taskDao;
    private LiveData<List<TaskEntity>> allTasks;

    TaskListRepository(Application application) {
        TaskListDatabase taskListDatabase = TaskListDatabase.getDatabase(application);
        taskDao = taskListDatabase.taskDao();
        allTasks = taskDao.getAllTasks();
    }

    LiveData<List<TaskEntity>> getAllTasks() {
        return allTasks;
    }

    public void insert(TaskEntity taskEntity) {
        new insertAsyncTask(taskDao).execute(taskEntity);
    }

    private static class insertAsyncTask extends AsyncTask<TaskEntity, Void, Void> {
        private TaskDao taskDao;

        insertAsyncTask(TaskDao task) {
            taskDao = task;
        }

        @Override
        protected Void doInBackground(TaskEntity... taskEntities) {
            taskDao.insert(taskEntities[0]);
            return null;

        }
    }

    public void update(TaskEntity taskEntity) {
        new updateAscyncTask(taskDao).execute(taskEntity);
    }

    private static class updateAscyncTask extends AsyncTask<TaskEntity, Void, Void> {
        private TaskDao taskDao;

        updateAscyncTask(TaskDao task) {
            taskDao = task;
        }

        @Override
        protected Void doInBackground(TaskEntity... taskEntities) {
            taskDao.update(taskEntities[0]);
            return null;
        }
    }

    public void delete(TaskEntity taskEntity) {
        new deleteAscyncTask(taskDao).execute(taskEntity);
    }

    private static class deleteAscyncTask extends AsyncTask<TaskEntity, Void, Void> {
        private TaskDao taskDao;

        deleteAscyncTask(TaskDao task) {
            taskDao = task;
        }

        @Override
        protected Void doInBackground(TaskEntity... taskEntities) {
            taskDao.delete(taskEntities[0]);
            return null;
        }
    }

}
