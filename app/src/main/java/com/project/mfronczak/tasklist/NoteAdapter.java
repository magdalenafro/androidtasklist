package com.project.mfronczak.tasklist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.mfronczak.tasklist.databasemanagement.model.TaskEntity;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.Viewholder> {

    private List<TaskEntity> tasksList;
    private final OnClickListener onClickListener;

    NoteAdapter(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    //get information from xml about view
    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_task, viewGroup, false);
        return new Viewholder(view);
    }

    // definition of concret item on screen
    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int i) {
        if (tasksList != null) {
            final TaskEntity taskEntity = tasksList.get(i);
            viewholder.name.setText(taskEntity.nameOfTask);
            viewholder.priority.setText(taskEntity.priority);

            viewholder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(taskEntity);
                }
            });
        } else
            viewholder.name.setText("No task to display");
    }

    void setTasksList(List<TaskEntity> tasksListToDisplay) {
        tasksList = tasksListToDisplay;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (tasksList != null) {
            return tasksList.size();
        } else {
            return 1;
        }

    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView imageView;
        public TextView priority;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            imageView = itemView.findViewById(R.id.star);
            priority = itemView.findViewById(R.id.priority);
        }
    }

    public interface OnClickListener {
        void onClick(TaskEntity taskEntity);
    }
}
