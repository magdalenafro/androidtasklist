package com.project.mfronczak.tasklist;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.project.mfronczak.tasklist.databasemanagement.TaskViewModel;
import com.project.mfronczak.tasklist.databasemanagement.model.TaskEntity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements NoteAdapter.OnClickListener {

    private static final int NEW_TASK_ACTIVITY_REQUEST_CODE = 1;
    private static final int UPDATE_TASK_ACTIVITY_REQUEST_CODE = 2;
    public static final String CURRENT_TASK = "CURRENT_TASK";
    private TaskViewModel mTaskViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycleeView);
        final NoteAdapter noteAdapter = new NoteAdapter(this);
        recyclerView.setAdapter(noteAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);

        mTaskViewModel.getAllTasks().observe(this, new Observer<List<TaskEntity>>() {
            @Override
            public void onChanged(@Nullable List<TaskEntity> taskEntities) {
                noteAdapter.setTasksList(taskEntities);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTaskActivity.class);
                startActivityForResult(intent, NEW_TASK_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_TASK_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            mTaskViewModel.insert((TaskEntity) data.getSerializableExtra(NewTaskActivity.EXTRA_REPLY));
        } else if (requestCode == UPDATE_TASK_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            mTaskViewModel.update((TaskEntity) data.getSerializableExtra(UpdateTaskActivity.UPDATE_TASK));
        } else {
            Toast.makeText(getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(TaskEntity taskEntity) {
        Intent intent = new Intent(MainActivity.this, UpdateTaskActivity.class);
        intent.putExtra(CURRENT_TASK, taskEntity);
        startActivityForResult(intent, UPDATE_TASK_ACTIVITY_REQUEST_CODE);

    }
}
