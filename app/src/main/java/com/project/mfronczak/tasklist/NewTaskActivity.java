package com.project.mfronczak.tasklist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.project.mfronczak.tasklist.databasemanagement.model.TaskEntity;

public class NewTaskActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public static final String EXTRA_REPLY = "REPLY";
    private EditText nameValue;
    private EditText descriptionValue;
    private Spinner prioritySpinner;
    private ArrayAdapter<String> priorityAdapter;
    private String[] priorityValues = new String[]{
            "1", "2", "3"
    };
    private Button buttonAccept;
    private Button buttonDeny;

    //private final TaskDao testTaskDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        Intent intent = getIntent();

        nameValue = findViewById(R.id.nameValue);
        descriptionValue = findViewById(R.id.descriptionValue);
        prioritySpinner = findViewById(R.id.priority_spinner);

        getSupportActionBar().setTitle(R.string.titleNewTaskActivity);

        priorityAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                priorityValues);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySpinner.setAdapter(priorityAdapter);
        prioritySpinner.setOnItemSelectedListener(this);

        buttonAccept = findViewById(R.id.accept);
        buttonDeny = findViewById(R.id.deny);

        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(nameValue.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    TaskEntity taskEntity = getTaskEntity();
                    replyIntent.putExtra(EXTRA_REPLY, taskEntity);
                    setResult(RESULT_OK, replyIntent);
                }

                //NewTaskActivity.super.onBackPressed();
                finish();
            }
        });

        buttonDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //NewTaskActivity.super.onBackPressed();
                finish();
            }
        });


    }

    @NonNull
    private TaskEntity getTaskEntity() {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setNameOfTask(nameValue.getText().toString());
        taskEntity.setNotes(descriptionValue.getText().toString());
        return taskEntity;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.e("abcde", "clicked");

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Log.e("abcde", "clickedNOT");
    }


}
