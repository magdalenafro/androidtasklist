package com.project.mfronczak.tasklist;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.project.mfronczak.tasklist.databasemanagement.TaskViewModel;
import com.project.mfronczak.tasklist.databasemanagement.model.TaskEntity;

public class UpdateTaskActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public static final String UPDATE_TASK = "UPDATE_TASK";
    public static final String REMOVE_TASK = "REMOVE_TASK";
    private TextView nameValue;
    private EditText descriptionValue;
    private Spinner prioritySpinner;
    private ArrayAdapter<String> priorityAdapter;
    private String[] priorityValues = new String[]{
            "1", "2", "3"
    };
    private Button buttonAccept;
    private Button buttonDeny;
    private TaskEntity editedTask;
    private Button buttonRemoveTask;
    private TaskViewModel mTaskViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_task);
        editedTask = (TaskEntity) getIntent().getSerializableExtra(MainActivity.CURRENT_TASK);

        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        Intent intent = getIntent();

        nameValue = findViewById(R.id.nameValue);
        nameValue.setText(editedTask.getNameOfTask());
        descriptionValue = findViewById(R.id.descriptionValue);
        descriptionValue.setText(editedTask.getNotes());
        prioritySpinner = findViewById(R.id.priority_spinner);

        getSupportActionBar().setTitle(R.string.titleUpdateActivity);

        priorityAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                priorityValues);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySpinner.setAdapter(priorityAdapter);
        prioritySpinner.setOnItemSelectedListener(this);

        buttonAccept = findViewById(R.id.accept);
        buttonDeny = findViewById(R.id.deny);
        buttonRemoveTask = findViewById(R.id.removeTask);

        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(descriptionValue.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    TaskEntity taskEntity = new TaskEntity();
                    taskEntity.setNotes(descriptionValue.getText().toString());
                    replyIntent.putExtra(UPDATE_TASK, taskEntity);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });

        buttonDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonRemoveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTaskViewModel.remove(editedTask);

                finish();
            }
        });




    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.e("abcde", "clicked");

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Log.e("abcde", "clickedNOT");
    }

}
