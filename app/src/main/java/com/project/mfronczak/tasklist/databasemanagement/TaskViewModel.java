package com.project.mfronczak.tasklist.databasemanagement;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.project.mfronczak.tasklist.databasemanagement.model.TaskEntity;

import java.util.List;

public class TaskViewModel extends AndroidViewModel {
    private TaskListRepository taskListRepository;
    private LiveData<List<TaskEntity>> allTasks;

    public TaskViewModel(Application application) {
        super(application);
        taskListRepository = new TaskListRepository(application);
        allTasks = taskListRepository.getAllTasks();
    }

    public LiveData<List<TaskEntity>> getAllTasks() {
        return allTasks;
    }

    public void insert(TaskEntity taskEntity) {
        taskListRepository.insert(taskEntity);
    }
    public void update(TaskEntity taskEntity) {taskListRepository.update(taskEntity);}
    public void remove(TaskEntity taskEntity) {taskListRepository.delete(taskEntity);}


}
