package com.project.mfronczak.tasklist.databasemanagement;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.project.mfronczak.tasklist.databasemanagement.model.TaskDao;
import com.project.mfronczak.tasklist.databasemanagement.model.TaskEntity;

@Database(entities = {TaskEntity.class}, version = 1)
public abstract class TaskListDatabase extends RoomDatabase {
    public abstract TaskDao taskDao();

    private static volatile TaskListDatabase INSTANCE;

    //Make the TaskListDatabase singleton
    static TaskListDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TaskListDatabase.class) {
                if (INSTANCE == null) {
                    //Creation of database
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), TaskListDatabase.class, "task_list_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

            new PopulateDbAsync(INSTANCE).execute();
        }
    };


    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final TaskDao taskDao;

        PopulateDbAsync(TaskListDatabase db){
            taskDao = db.taskDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
          //  taskDao.insert(buildTestTask());
            return null;
        }

        private TaskEntity buildTestTask() {
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setNameOfTask("test");
            taskEntity.setAddedDate(123456);
            taskEntity.setId(2);
            return taskEntity;
        }
    }

}
